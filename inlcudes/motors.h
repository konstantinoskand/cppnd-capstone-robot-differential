#ifndef MOTORSMODULE_H
#define MOTORSMODULE_H

/******************************************************************************
***************************   Includes   **************************************
******************************************************************************/
#include <iostream>
#include <memory>
#include <math.h>
#include <fstream>
#include <string>
#include <ctime>

/******************************************************************************
***************************  Declarations *************************************
******************************************************************************/
/**
 * @brief Motors Class Constructor - The Motors class is responsible for the motors movement
 * @author konstantinoskand
 */
class Motors
{
public:
    /**
     * @brief Motors Class Constructor - The Motors class is responsible for the motors movement
     * @author konstantinoskand
     */
    Motors();

     /**
     * @brief Get-Function for the motor ID number
     * @return int
     * @author konstantinoskand
     */
    int getID();
    /**
     * @brief motors function for writing in file the motor speed
     * @param float motorSpeed: motor speed in rpm
     * @param float angle: Steering angle
     * @return void
     * @author konstantinoskand
     */
    void move(float motorSpeed, float angle);

private:
    /* Every Motors object has its own unique id */
    int _id;
    /* Global variable for counting object ids */
    static int _idCnt;
    /* Name of the output file */
    std::string _outfileName;
};

#endif // MOTORSMODULE_H

/******************************************************************************
********************************  End  ****************************************
******************************************************************************/