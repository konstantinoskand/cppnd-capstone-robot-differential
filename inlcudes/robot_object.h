#ifndef ROBOTOBJECT_H
#define ROBOTOBJECT_H

/******************************************************************************
***************************   Includes   **************************************
******************************************************************************/
#include <vector>
#include <iostream>
#include <stdexcept>
#include <math.h>
#include "motors.h"

/******************************************************************************
***************************  Declarations *************************************
******************************************************************************/
/**
 * @brief enumeration for the Robot Types (twoWheelDrive/fourWheelDrive/noType)
 * @author konstantinoskand
 */
enum RobotType{
    noType,
    twoWheelDrive,
    fourWheelDrive,
};

/**
 * @brief The Robot Object class is responsible for a robot object.
 * @author konstantinoskand
 */
class RobotObject : public Motors
{
public:
    /**
     * @brief Default Constructor
     * @author konstantinoskand
     */
    RobotObject();
    /**
     * @brief The Robot Object class is responsible for a robot 
     * object. It's target is to create 2wd/4wd robot objects and calculate
     * the differential robot speed.
     * @param RobotType robotType: The type of the robot->2wd/4wd
     * @author konstantinoskand
     */
    RobotObject(RobotType robotType);

    /**
     * @brief Get-Function for the Robot ID number
     * @return int
     * @author konstantinoskand
     */
    int getID() { return _id; };
    /**
     * @brief Get-Function for the Robot type (2wd/4wd/NoType)
     * @return RobotType
     * @author konstantinoskand
     */
    RobotType getType() { return _type; };
    /**
     * @brief Get-Function for the reference Robot's Front-Left motor angular speed
     * @return float
     * @author konstantinoskand
     */
    float getFL_angular_speed() { return _fmleft_angular_speed; };
    /**
     * @brief Get-Function for the reference Robot's Front-Right motor angular speed
     * @return float
     * @author konstantinoskand
     */
    float getFR_angular_speed() { return _fmright_angular_speed; };
    /**
     * @brief Get-Function for the reference Robot's Rear-Left motor angular speed
     * @return float
     * @author konstantinoskand
     */
    float getRL_angular_speed() { return _rmleft_angular_speed; };
    /**
     * @brief Get-Function for the reference Robot's Rear-Right motor angular speed
     * @return float
     * @author konstantinoskand
     */
    float getRR_angular_speed() { return _rmright_angular_speed; };
    
    /**
     * @brief Differential speed realization for each of the 2wd/4wd robot motors
     * @param float angle: steering angle value in degrees
     * @param float vehicle_speed: vehicle cg speed in m/s
     * @param RobotType robotType: Type of the robot (2wd/4wd)
     * @return void
     * @author konstantinoskand
     */
    void robot_differential_speed_calc(float angle, float vehicle_speed, RobotType robotType);

private:
    /* Every robot object has its own unique id */
    int _id;
    /* Robot type (2-wheeled or 4-wheeled) */
    RobotType _type;
    /* Global variable for counting object ids */
    static int _idCnt;
    /* Steering angle in rads */
    float _anglesInRads;
    /* Wheelbase length in meters */
    float _wheelBase;
    /* Trackwidth length in meters */
    float _trackWidth;
    /* Desired steering angle in rad */
    float _steering_Desired_Angle;
    /* Previous value of Steering angle in rad */
    float _steering_Desired_Angle_Last;
    /* Wheel radius in meters */
    float _wheel_Radius;
    /* Ackerman turning circle radius for FRONT LEFT wheel in meters */
    float _fleft_radius;
    /* Ackerman turning circle radius for FRONT RIGHT wheel in meters */
    float _fright_radius;
    /* Ackerman turning circle radius for REAR LEFT wheel in meters */
    float _rleft_radius;
    /* Ackerman turning circle radius for REAR RIGHT wheel in meters */
    float _rright_radius;
    /* Ackerman turning circle radius for robot center of gravity */
    float _cg_radius;
    /* Differential FRONT LEFT desired speed in m/sec */
    float _fmleft_speed;
    /* Differential FRONT RIGHT desired speed in m/sec */
    float _fmright_speed;
    /* Differential REAR LEFT desired speed in m/sec */
    float _rmleft_speed;
    /* Differential REAR RIGHT desired speed in m/sec */
    float _rmright_speed;
    /* Differential robot center of gravity desired angular speed in rpm */
    float _cg_angular_speed;
    /* Differential FRONT LEFT wheel desired angular speed in rpm */
    float _fmleft_angular_speed;
    /* Differential FRONT RIGHT wheel desired angular speed in rpm */
    float _fmright_angular_speed;
    /* Differential REAR LEFT wheel desired angular speed in rpm */
    float _rmleft_angular_speed;
    /* Differential REAR RIGHT wheel desired angular speed in rpm */
    float _rmright_angular_speed;
    /* Vector of Robot Motor objects */
    std::vector<std::unique_ptr<Motors>> _motors;
};

#endif // ROBOTOBJECT_H

/******************************************************************************
********************************  End  ****************************************
******************************************************************************/