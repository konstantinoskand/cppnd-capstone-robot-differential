/******************************************************************************
***************************   Includes   **************************************
******************************************************************************/
#include "../inlcudes/motors.h"

/******************************************************************************
***************************    Defines  ***************************************
******************************************************************************/

/* Initialization of the motors object number (static variable) */
int Motors::_idCnt = 0;

/******************************************************************************
***************************  Functions ****************************************
******************************************************************************/
/**
 * @brief Motors Class Constructor - The Motors class is responsible for the motors movement
 * @author konstantinoskand
 */
Motors::Motors()
{
  /* At every object creation -> Update the object ID */
  _id = _idCnt++;
  std::time_t result = std::time(nullptr);
  
  
  /* Creating a different file name for each motor object */
  _outfileName = "../output_data/motor_" + std::to_string(_id) + " " + std::asctime(std::localtime(&result)) + ".txt";
}

/**
 * @brief motors function for writing in file the motor speed
 * @param float motorSpeed: motor speed in rpm
 * @param float angle: Steering angle
 * @return void
 * @author konstantinoskand
 */
void Motors::move(float motorSpeed, float angle)
{
  std::ofstream outfile (this->_outfileName, std::fstream::app);
  outfile << "For a desired steering angle: " << angle << " degs" << " - Calculated motor speed is: "  << motorSpeed << " rmp" << std::endl;
}

/******************************************************************************
********************************  End  ****************************************
******************************************************************************/