/******************************************************************************
***************************   Includes   **************************************
******************************************************************************/
#include "../inlcudes/robot_object.h"

/******************************************************************************
***************************    Defines  ***************************************
******************************************************************************/
/* Factor * rads = degs */
#define RAD_TO_DEG_FACTOR       (0.104719755)
#define DEG_TO_RAD              (0.0174532925)

/* Initialization of the robots objects' number (static variable) */
int RobotObject::_idCnt = 0;

/******************************************************************************
***************************  Functions ****************************************
******************************************************************************/
/**
 * @brief from_radians_per_second_to_rpm converts an angular speed from rad/sec
 * to rpm
 * @param rad/sec: rad/sec value
 * @return float rpm value
 * @author konstantinoskand
 */
inline const float from_radians_per_second_to_rpm(const float rad_per_second)
{
  return (rad_per_second * RAD_TO_DEG_FACTOR);
}

/**
 * @brief from_deg_to_rad converts degrees to radians
 * @param deg: degrees value
 * @return float radians value
 * @author konstantinoskand
 */
inline const float from_deg_to_rad(const float degrees)
{
  return (degrees * DEG_TO_RAD);
}

/**
 * @brief limiting the value to the min max values given as arguments
 * @param value to be limited
 * @param float min: min limit value
 * @param float max: max limit value
 * @return float value: limited value
 * @author konstantinoskand
 */
inline const float limit_value_min_max_(float value, const float min, const float max)
{
  /* If value is lower than min limit... */
  if(value < min)
  {
    /* Limit value to the min limit */
    value = min;
  }
  /* If value is bigger than max limit... */
  else if(value > max)
  {
    /* Limit value to the max limit */
    value = max;
  }

  /* Return limited value */
  return value;
}

/**
 * @brief The Robot Object class is responsible for a robot 
 * object. It's target is to create 2wd/4wd robot objects and calculate
 * the differential robot speed.
 * @param RobotType robotType: The type of the robot->2wd/4wd
 * @author konstantinoskand
 */
RobotObject::RobotObject(RobotType robotType)
{
    /* Robot type initialized by given argument in main */
    _type = robotType;
    /* At every object creation -> Update the object ID */
    _id = _idCnt++;
    /* Wheelbase length in meters */
    _wheelBase = 0.305;
    /* Trackwidth length in meters */
    _trackWidth = 0.325;
    /* Ackerman turning circle radius for FRONT LEFT wheel in meters */
    _fleft_radius = 0;
    /* Ackerman turning circle radius for FRONT RIGHT wheel in meters */
    _fright_radius = 0;
    /* Ackerman turning circle radius for REAR LEFT wheel in meters */
    _rleft_radius = 0;
    /* Ackerman turning circle radius for REAR RIGHT wheel in meters */
    _rright_radius = 0;
    /* Ackerman turning circle radius for robot center of gravity */
    _cg_radius = 0;
    /* Differential FRONT LEFT desired speed in m/sec */
    _fmleft_speed = 0;
    /* Differential FRONT RIGHT desired speed in m/sec */
    _fmright_speed = 0;
    /* Differential REAR LEFT desired speed in m/sec */
    _rmleft_speed = 0;
    /* Differential REAR RIGHT desired speed in m/sec */
    _rmright_speed = 0;
    /* Differential robot center of gravity desired angular speed in rpm */
    _cg_angular_speed = 0;
    /* Differential FRONT LEFT wheel desired angular speed in rpm */
    _fmleft_angular_speed = 0;
    /* Differential FRONT RIGHT wheel desired angular speed in rpm */
    _fmright_angular_speed = 0;
    /* Differential REAR LEFT wheel desired angular speed in rpm */
    _rmleft_angular_speed = 0;
    /* Differential REAR RIGHT wheel desired angular speed in rpm */
    _rmright_angular_speed = 0;
    /* Wheel radius in meters */
    _wheel_Radius = 0.13716;
    /* Steering Angle in rads  */
    _anglesInRads = 0;

    /* If robot type is 4wd create 4 Motors objects */
    if(_type == RobotType::fourWheelDrive)
    {
      /* One motor object per wheel = In total 4 motor objects */
      for(auto i = 0; i <= 4; ++i)
      {
        /* Create the motor object and append it to the Motors object vector */
        _motors.push_back(std::unique_ptr<Motors>(new Motors));
      }
    }
    /* else if the type is 2wd create 2 Motors objects */
    else if(_type == RobotType::twoWheelDrive)
    {
      /* One motor object per wheel = In total 2 motor objects */
      for(auto i = 0; i <= 2; ++i)
      {
        /* Create the motor object and append it to the Motors object vector */
        _motors.push_back(std::unique_ptr<Motors>(new Motors));
      }
        
    }
    /* else Robot object is noType */
    else
    {
      /* Print error message */
      throw std::invalid_argument( "received NO ROBOT TYPE" );
    }
}

/**
 * @brief Differential speed realization for each of the 2wd/4wd robot motors
 * @param float angle: steering angle value in degrees
 * @param float vehicle_speed: vehicle cg speed in m/s
 * @param RobotType robotType: Type of the robot (2wd/4wd)
 * @return void
 * @author konstantinoskand
 */
void RobotObject::robot_differential_speed_calc(float angle, float vehicle_speed, RobotType robotType)
{
  /* Calculate desired steering in rads */
  _anglesInRads = from_deg_to_rad(angle);

  /* if angle is 0 the ackerman model has undefined behavior and it is not needed as there is no turn... */
  if(_anglesInRads == 0)
  {
    /* All motor speeds the same with the vehicle speed -> Maintain same vehicle speed */
    _fmleft_angular_speed = vehicle_speed;
    _fmright_angular_speed = vehicle_speed;
    _rmleft_angular_speed = vehicle_speed;
    _rmright_angular_speed = vehicle_speed;
  }
  /* Ackerman's model shall be used... */
  else
  {
    /* Center of gravity steering radius calculation */
    _cg_radius = _wheelBase / tan(_anglesInRads);
    _cg_radius *= _cg_radius;
    _cg_radius += ((_wheelBase * _wheelBase) / 4);
    _cg_radius = sqrt(_cg_radius);

    /* Front left wheel steering radius calculation */
    _fleft_radius = _wheelBase / tan(_anglesInRads);
    _fleft_radius -= (_trackWidth / 2);
    _fleft_radius *= _fleft_radius;
    _fleft_radius += (_wheelBase * _wheelBase);
    _fleft_radius = sqrt(_fleft_radius);

    /* Front right wheel steering radius calculation */
    _fright_radius = _wheelBase / tan(_anglesInRads);
    _fright_radius += (_trackWidth / 2);
    _fright_radius *= _fright_radius;
    _fright_radius += (_wheelBase * _wheelBase);
    _fright_radius = sqrt(_fright_radius);

    /* Rear left wheel steering radius calculation */
    _rleft_radius = _wheelBase / tan(_anglesInRads);
    _rleft_radius -= (_trackWidth / 2);

    /* Rear right wheel steering radius calculation */
    _rright_radius = _wheelBase / tan(_anglesInRads);
    _rright_radius += (_trackWidth / 2);

    /* Center of Gravity angular speed calculation */
    _cg_angular_speed = vehicle_speed / _cg_radius;

    /* Front left motor speed calculation */
    _fmleft_speed = _cg_angular_speed * _fleft_radius;
    /* Front right motor speed calculation */
    _fmright_speed = _cg_angular_speed * _fright_radius;
    /* Rear left motor speed calculation */
    _rmleft_speed = _cg_angular_speed * _rleft_radius;
    /* Rear right motor speed calculation */
    _rmright_speed = _cg_angular_speed * _rright_radius;

    /* Front left motor angular speed calculation */
    _fmleft_angular_speed = _fmleft_speed / _wheel_Radius;
    /* Front right motor angular speed calculation */
    _fmright_angular_speed = _fmright_speed / _wheel_Radius;
    /* Rear left motor angular speed calculation */
    _rmleft_angular_speed = _rmleft_speed / _wheel_Radius;
    /* Rear right motor angular speed calculation */
    _rmright_angular_speed = _rmright_speed / _wheel_Radius;

    /* convert angular speeds from rad/sec to rpm */
    _fmleft_angular_speed = from_radians_per_second_to_rpm(_fmleft_angular_speed);
    _fmright_angular_speed = from_radians_per_second_to_rpm(_fmright_angular_speed);
    _rmleft_angular_speed = from_radians_per_second_to_rpm(_fmleft_angular_speed);
    _rmright_angular_speed = from_radians_per_second_to_rpm(_rmright_angular_speed);
  }

  /* If the robot is 2wd -> only front wheels motors' movement */
  if(getType() == RobotType::twoWheelDrive)
  {
    /* Write the each motor speed in its file */
    _motors[0]->move(_fmleft_angular_speed, angle);
    _motors[1]->move(_fmright_angular_speed, angle);
  }
  /* 4wd Robot -> 4 motors' movement */
  else if(getType() == RobotType::fourWheelDrive)
  {
    /* Write the each motor speed in its file */
    _motors[0]->move(_fmleft_angular_speed, angle);
    _motors[1]->move(_fmright_angular_speed, angle);
    _motors[2]->move(_rmleft_angular_speed, angle);
    _motors[3]->move(_rmright_angular_speed, angle);
  }
  /* No robot type... */
  else
  {
    /* Stop and print an error */
    throw std::invalid_argument( "NO ROBOT TYPE" );
  }
}

/******************************************************************************
********************************  End  ****************************************
******************************************************************************/