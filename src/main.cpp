/******************************************************************************
***************************   Includes   **************************************
******************************************************************************/
#include <iostream>
#include <fstream>
#include <string>
#include "../inlcudes/robot_object.h"

/******************************************************************************
***************************    Defines  ***************************************
******************************************************************************/

/******************************************************************************
***************************  Functions ****************************************
******************************************************************************/
int main() {
    std::cout << "Program Started" << std::endl;

    /*  Initializing vehicle speed - This value would be an input from an accelerometer or gps 
        but we assume it is stable linear velocity for our robot (in m/sec) */
    float vehicle_speed = 7;
    /* Creating a file stream in order to read the angles from a file */
    std::ifstream angleFromFile;
    /* Initializing an empty float in order to get the angle value from the file */
    float angle;

    /* Opening the angle.txt which contains a specific angle value in each line */
    angleFromFile.open("../input_data/angle.txt");

    /* Creating a Robot object */
    std::unique_ptr<RobotObject> Robot = std::unique_ptr<RobotObject>(new RobotObject(RobotType::fourWheelDrive));

    /* If the file is open... */
    if(angleFromFile.is_open())
    {
        std::cout << "File Opened!" << std::endl;
        /* While stream is good (e.g not at the end of the file)... */
        while(angleFromFile)
        {
            angleFromFile >> angle;
            /* Calculate the differential speeds for our motor */
            Robot->robot_differential_speed_calc(angle, vehicle_speed, Robot->getType());
        }
    }
    else
    {
        std::cout << "Error opening the file: angle.txt" << std::endl;
    }

    /* Before exit close the file */
    angleFromFile.close();

    return 0;
}

/******************************************************************************
********************************  End  ****************************************
******************************************************************************/