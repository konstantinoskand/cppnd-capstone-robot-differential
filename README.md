# CPPND: Capstone Robot Differential

This is a Capstone project in the [Udacity C++ Nanodegree Program](https://www.udacity.com/course/c-plus-plus-nanodegree--nd213).

The Capstone Project gives you a chance to integrate what you've learned throughout this program.

## Rubric:

The following rubric points were covered:

* README: All Rubric Points COVERED
* Compiling and Testing: All Rubric Points COVERED
* Loops, Functions, I/O:
    * The project demonstrates an understanding of C++ functions and control structures: COVERED
    * The project reads data from a file and process the data, or the program writes data to a file: COVERED
* Object Oriented Programming:
    * The project uses Object Oriented Programming techniques: COVERED
    * Classes use appropriate access specifiers for class members: COVERED
    * Classes abstract implementation details from their interfaces: COVERED
    * Classes encapsulate behavior: COVERED
    * Classes follow an appropriate inheritance hierarchy: COVERED
* Memory Management:
    * The project uses destructors appropriately: COVERED
    * The project uses smart pointers instead of raw pointers: COVERED

## Overview

The project implements a differential speed calculator for the robot wheel-motors based on the Ackermann's model of steering.

The input is the the desired steering angle, which in this case is readed from a file "/input_data/angle.txt".

The outputs, based on the robot type (2wd/4wd), are the two or four differential speeds calculated based on the desired steering angle and written in separate files for each motor.

More information about the Ackermann's steering geometry can be found in the following link:
(https://en.wikipedia.org/wiki/Ackermann_steering_geometry)

## Architecture

* input_data:  It contains the .txt file of the desired angle and this angles represents the input to the system of the desired steering behaviour.
* output_data: It contains the .txt files for each motor depending on the robot type - 2 files if the robot is 2wd or 4 files if the robot is 4wd.
* src: Here is the source code which contains the following files:
    * main.cpp: The main system module.
    * motors.cpp: This module is responsible for the motors movement. In this case the speeds are written in separate files for each motor.
    * robot_object.cpp: This module is responsible for the robot object and the differential speeds' calculation.
* inlcudes: Here are the header fiels needed for the delcaration of the robot and motors classes
    * motors.h: Header declarations related to the motors class
    * robot_object.h: Header declarations related to the robot class

## Dependencies for Running Locally
* cmake >= 3.7
  * All OSes: [click here for installation instructions](https://cmake.org/install/)
* make >= 4.1 (Linux, Mac), 3.81 (Windows)
  * Linux: make is installed by default on most Linux distros
  * Mac: [install Xcode command line tools to get make](https://developer.apple.com/xcode/features/)
  * Windows: [Click here for installation instructions](http://gnuwin32.sourceforge.net/packages/make.htm)
* gcc/g++ >= 5.4
  * Linux: gcc / g++ is installed by default on most Linux distros
  * Mac: same deal as make - [install Xcode command line tools](https://developer.apple.com/xcode/features/)
  * Windows: recommend using [MinGW](http://www.mingw.org/)

## Basic Build Instructions

1. Clone this repo.
2. Make a build directory in the top level directory: `mkdir build && cd build`
3. Compile: `cmake .. && make`
4. Run it: `./RobotDifferential`.