cmake_minimum_required(VERSION 3.7)

add_definitions(-std=c++17)

set(CXX_FLAGS "-Wall")
set(CMAKE_CXX_FLAGS, "${CXX_FLAGS}")

project(RobotDifferential)

include_directories(${PROJECT_SOURCE_DIR})
add_executable(RobotDifferential src/main.cpp src/robot_object.cpp src/motors.cpp)
